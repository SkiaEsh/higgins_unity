﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosionFade : MonoBehaviour
{

    //The time and speed the explosion is faded.
    public float speed = 0f;
    public float time = 0.5f;
    //Uses sprite renderer on object to access color.
    private SpriteRenderer explosionRenderer;

    // Use this for initialization
    void Start ()
    {
        //Gets the spriterenderer component and sets the color.
        explosionRenderer = GetComponent<SpriteRenderer>();
        explosionRenderer.color = new Color(1f, 1f, 1f, 1f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Alpha value rate and how fast it fades to transparency.
        float fadingExplosion = Mathf.SmoothDamp(explosionRenderer.color.a, 0f, ref speed, time);
        //Fades the color to transparency.
        explosionRenderer.color = new Color(1f, 1f, 1f, fadingExplosion);
        //Destroys the explosion after it fully fades.
        if (gameObject.name == "explosion(Clone)")
        {
            Destroy(gameObject, 1.0f);
        }
    }
}

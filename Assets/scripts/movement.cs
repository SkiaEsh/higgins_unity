﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    //The speed of the ship and its angle for turning.
    public float speed = 10.0F;
    public float angleDir;
    //Music audio source; music will loop
    private AudioSource music;


    void Awake()
    {

    }

    // Use this for initialization
    void Start ()
    {
        //Gets the audio source and it should play upon game's start.
        music = GetComponent<AudioSource>();
    }
    //Error checking code commented out for final version
    void OnGUI()
    {
        //GUI.Label(new Rect(0, 0, 100, 100), angleDir.ToString() + '\n' + eulerAngleDir.ToString() + '\n' + m_Rigidbody.velocity.ToString() + '\n' + transform.forward.ToString());
    }

    // Update is called once per frame
    void Update ()
    {
        //Changes the value to negative if it is greater than zero to make ship move in the right direction when turning.
        angleDir = transform.rotation.z;
        
        if (angleDir > 0)
        {
            angleDir = angleDir * -1;
        }

        //Rotates the ship right.  The left key does the same in the opposite direction.
        if (Input.GetKeyDown(KeyCode.RightArrow) ||
            Input.GetKey(KeyCode.RightArrow))
        {
             transform.Rotate((0), (0), (360) * (speed * Time.deltaTime));
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) ||
            Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate((0), (0), (360) * (-speed * Time.deltaTime));
        }

        //Moves the ship forward in the direction it is facing.
        if (Input.GetKeyDown(KeyCode.UpArrow) ||
            Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += transform.up * (angleDir * speed);
        }

        // X-axis limit that prevents the ship from leaving the screen by moving it back in so quickly it is unnoticeable by a viewer.
        if (transform.position.x <= -8.0f)
        {
            transform.position = new Vector2(-8.0f, transform.position.y);
        }
        else if (transform.position.x >= 8.0f)
        {
            transform.position = new Vector2(8.0f, transform.position.y);
        }

        // Y-axis limit that works similarly to the X-Axis one.
        if (transform.position.y <= -4.2f)
        {
            transform.position = new Vector2(transform.position.x, -4.2f);
        }
        else if (transform.position.y >= 4.2f)
        {
            transform.position = new Vector2(transform.position.x, 4.2f);
        }

    }
}

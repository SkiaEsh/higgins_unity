﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidCreation : MonoBehaviour
{
    //Asteroid object will be cloned 4-6 times.
    public GameObject asteroid;
    //public GameObject bullet;
    //public float random;
    //Holds an int of 4-6 that is distributed between right and lefts sides.
    public int numberOfAsteroids;
    public int numberOfAsteroidsLeft;
    public int numberOfAsteroidsRight;
   // public GameObject asteroidCopies;
   //Check if asteroids have loaded; if not, it loads them.
    public bool asteroidsLoaded = false;
    //Holds asteroid clones in correct positions.
    public GameObject[] movingAsteroids;
    //Keeps count of above array's position during instantiation and adding asteroids to it.
    public int counter = 0;
    //Speed asteroids move.
    public float speed = 1.0f;

    //Creates 4-6 asteroids.
    void Awake()
    {
        SpawnAsteroid();
    }

    // Use this for initialization
    void Start()
    {

    }

    void SpawnAsteroid()
    {
        // Get random whole number of asteroids (max is exclusive), so 4-6.
        numberOfAsteroids = Mathf.RoundToInt(Random.Range(4.0f, 7.0f));

        // Creates that number of game objects.
        movingAsteroids = new GameObject[numberOfAsteroids];

        //Determines the number of asteroids on the right and left side.
        numberOfAsteroidsLeft = Mathf.RoundToInt(numberOfAsteroids / 2.0f);
        numberOfAsteroidsRight = Mathf.RoundToInt(numberOfAsteroids - numberOfAsteroidsLeft);


        //Spawns asteroids on left side and adds them to the array.  Counter keeps track of array's position.
        for (int i = 0; i < numberOfAsteroidsLeft; i++)
        {
            GameObject tempAsteroid = (Instantiate(asteroid,
                                                   new Vector3(Random.Range(-8.0f, -5.8f),
                                                               Random.Range(-4.2f, 4.2f),
                                                               0),
                                                   Quaternion.Euler(0, 0, Random.Range(-0.0f, 359.0f)))) as GameObject;

            movingAsteroids[counter] = tempAsteroid;
            counter++;


        }

        //Same as above.  Array starts from counter's last position from the above for loop.
        for (int i = 0; i < numberOfAsteroidsRight; i++)
        {
            GameObject tempAsteroid = (Instantiate(asteroid,
                                       new Vector3(Random.Range(8.0f, 5.8f),
                                                   Random.Range(-4.2f, 4.2f),
                                                   0),
                                       Quaternion.Euler(0, 0, Random.Range(-0.0f, 359.0f)))) as GameObject;

            movingAsteroids[counter] = tempAsteroid;
            counter++;
        }

        //Asteroids have force applied to move them.
        for (int i = 0; i < numberOfAsteroids; i++)
        {
            movingAsteroids[i].GetComponent<Rigidbody2D>().AddForce(-1 * movingAsteroids[i].transform.up * speed, ForceMode2D.Impulse);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
